const secret = process.env.SECRET_TOKEN;
const tiempo = process.env.TIME_TOKEN;
const requestJson = require('request-json');
var jwt = require('jsonwebtoken');

function isAuthenticatedV1(req, res, next){
  console.log("/apiraqueljorge/services/isAuthenticatedV1");

  var token = req.headers['tsec'];
  if (!token) {
    var response = {
            "msg" : "Unauthorized user"
         }
    return res.status(401).send(response);
  }

  jwt.verify(token, secret, function(err, decoded) {
      if (err){
        console.log(err);
        var response = {
                "msg" : "session expired"
             }
        return res.status(500).send(response);
      }
      req.idUser = decoded.idUser;
      next();
    });

}
function createToken(dato){
  console.log("/apiraqueljorge/services/createToken");
  var datos = {
    "idUser" : dato
  }
  var token = jwt.sign(datos, secret, {
      expiresIn: 86400 // expires in 24 hours
  });
  return token;
}
module.exports.isAuthenticatedV1 = isAuthenticatedV1;
module.exports.createToken = createToken;
