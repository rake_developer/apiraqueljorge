const crypt = require('../crypt.js');
const baseMLabURL = process.env.baseMLabURL;
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const requestJson = require('request-json');
const tknService = require('../services/TokenServices');


function loginV1(req, res){
console.log("POST /apiraqueljorge/v1/loginV1");

var httpClient = requestJson.createClient(baseMLabURL);

var query = 'q={"email" : "'+req.body.email+'"}';

httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err,resMLab, body){
    if(err){
      var response = {
        "msg" : "Error getting user. Try again please."
      }
      res.status(500).send(response);
    }else{
      if(body.length > 0){
        var response = body[0];
        if(crypt.checkPassword(req.body.password,body[0].password)){

          console.log("Password ok");

          var token = tknService.createToken(body[0].id);
          var response = {
            "msg" : "Login ok",
            "tsec" : token
          }
          res.status(200).send(response);

        }else{
          var response = {
            "msg" : "Error, password is not correct. Try again, please."
          }
          res.status(401).send(response);
        }
      }else{
        var response = {
          "msg" : "User not found"
        }
        res.status(404).send(response);
      }
    }

  }
);
}

function logoutV1(req, res){
  console.log("POST /apiraqueljorge/v1/logoutV1");

  var response = {
    "msg" : "Logout ok",
    "tsec" : null
  }
  res.status(200).send(response);
}
module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
