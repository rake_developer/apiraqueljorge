
const baseMLabURL = process.env.baseMLabURL;
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const requestJson = require('request-json');

function getAccountByIdV1(req,res){
  console.log("GET /apiraqueljorge/v1/account/:idAccount");

  var q = "q={id:" + req.params.idAccount + ",idUser:" + req.idUser + "}&";

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("account/?" + q + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {"msg" : "Error getting account"};
        res.status(500);
      }else{
        if (body.length > 0){
          var response = body;
        }else{
          var response = {"msg" : "Account not found"};
          res.status(404);
        }
      }
      res.send(response);
    });
}

function deleteAccountV1(req,res){
  console.log("DELETE /apiraqueljorge/v1/account/:idAccount");

  var idAccount = req.body.id;

  var q = "q={id:" + idAccount + ",idUser:" + req.idUser + "}&";

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("account/?" + q + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {"msg" : "Error getting account"};
        res.status(500).send(response);
      }else{
        if (body.length > 0){

          var putBody = '{"$set":{"activo":false}}';

          httpClient.put("account/" + q + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errP,resMLabP, bodyP){
              if(errP){
                var responseP = {
                  "msg" : "Error deleting account"
                }
                res.status(500).send(responseP);
              }else{
                if(bodyP.n>0){
                  var responseP = {
                    "msg" : "Account deleted"
                  }

                  res.status(200).send(responseP);
                }else{
                  var responseP = {
                    "msg" : "Account not found"
                  }
                  res.status(404).send(responseP);
                }
              }
            }
          );

        }else{
          var response = {"msg" : "Account not found"};
          res.status(404).send(response);
        }
      }

    });
}


function setAccountV1(req,res){
  console.log("POST /apiraqueljorge/v1/account/");
  var usuario = req.idUser;

  var httpClient = requestJson.createClient(baseMLabURL);
  var queryIBAN = 'q={iban : "'+req.body.iban+'"}';
  var queryMaxId = 'f={id:1}&s={id:-1}&l=1';
  var newId = 0;

  //[INICIO]Comprobamos que el IBAN no exista en la BD
  httpClient.get("account?" + queryIBAN + '&' + mLabAPIKey,
    function(err, resIban, bodyIban){
      if (err){
        res.status(500).send({"msg" : "Problems checking iban"})
      }else{
        if (bodyIban.length > 0){
          var response = {"msg" : "Existing IBAN"};
          res.status(400).send(response);
        }else{

          //[INICIO] Obtenemos el max id de cuentas
          httpClient.get("account?" + queryMaxId + '&' + mLabAPIKey,
            function(err, resMaxId, bodyId){
              newId = bodyId[0].id + 1;
              var newAccount = {
                "id" : newId,
                "iban": req.body.iban,
                "balance": 0,
                "idUser": usuario
              }
              httpClient.post('account?' + mLabAPIKey, newAccount,
                function(err, resAcc, bodyAcc){
                  if (err){
                    res.status(500).send({"msg" : "Problems creating account. Try again"})
                  }else{
                    console.log("Account created correctly");
                    res.status(201).send({"msg" : "Account created", "idAccount" : newId});
                  }
                });
              //[FIN] Crear cuenta

          });
          //[FIN] Obtenemos el max id de cuentas

        }
      }
    });
  //[FIN] Comprobar IBAN

}

function getUserAccounts(req,res){
  console.log("GET /apiraqueljorge/v1/account/");

  var q = "q={idUser:" + req.idUser + "}&";

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("account/?" + q + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {"msg" : "Error getting accounts"};
        res.status(500);
      }else{
        if (body.length > 0){
          var response = body;
        }else{
          var response = {"msg" : "Non-existing accounts"};
          res.status(404);
        }
      }
      res.send(response);
    });
}
module.exports.getAccountByIdV1 = getAccountByIdV1;
module.exports.setAccountV1 = setAccountV1;
module.exports.getUserAccounts = getUserAccounts;
module.exports.deleteAccountV1 = deleteAccountV1;
