const crypt = require('../crypt.js');
const baseMLabURL = process.env.baseMLabURL;
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const requestJson = require('request-json');
const tknService = require('../services/TokenServices');

function getUserByIdV1(req,res){
  console.log("GET /apiraqueljorge/v1/user/");

  var q = "q={id:" + req.idUser + "}&f={first_name:1,last_name:1,_id:0}&";

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + q + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {"msg" : "Error getting user"};
        res.status(500);
      }else{
        if (body.length > 0){
          var response = body[0];
        }else{
          var response = {"msg" : "User not found"};
          res.status(404);
        }
      }
      res.send(response);
    });
}


function setUserV1(req,res){
  //Comprobamos que no exista un usuario con el mismo correo.
  var query = 'q={"email" : "'+req.body.email+'"}';
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body){
      console.log("Comprobando si existe usuario");
      if(err){
        var response = {
          "msg" : "Server error. Try again please"
        }
        res.status(500).send(response);
      }else{
        if(body.length > 0){
          res.status(400).send({"msg" : "Email already exists"});
        }else{
          var queryMaxId = 'f={id:1}&s={id:-1}&l=1&';
          var newId = 0;
          httpClient.get("user?" + queryMaxId + mLabAPIKey,
            function(err, resMaxId, body){
              newId = body[0].id + 1;
              ///////Creamos al usuario en el caso de que no exista el correo
               var newUser = {
                 "id" : newId,
                 "first_name" :req.body.first_name ,
                 "last_name" : req.body.last_name,
                 "email" : req.body.email,
                 "dni" : req.body.dni,
                 "password" : crypt.hash(req.body.password)
               };
             httpClient.post("user?" + mLabAPIKey, newUser,
              function(err, resMLab, bodyInsert){
                if (err)
                  res.status(500).send({"msg" : "Server error. Try again please"});
                else{
                  var token = tknService.createToken(newId);
                  var response = {
                    "msg" : "User created",
                    "tsec" : token
                  }
                  res.status(201).send(response);
                }
              });
          ////////////////////////////////////////////////////////////
          });
        }
      }
  });
}

module.exports.getUserByIdV1 = getUserByIdV1;
module.exports.setUserV1 = setUserV1;
