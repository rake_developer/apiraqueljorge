const baseMLabURL = process.env.baseMLabURL;
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const requestJson = require('request-json');

function getMovementsByIdAccountV1(req,res){
  console.log("GET /apiraqueljorge/v1/account/:idAccount/movement");

  var httpClient = requestJson.createClient(baseMLabURL);

  //Comprobamos que la cuenta es del usuario conectado
  var q = "q={id:" + req.params.idAccount + ",idUser:" + req.idUser + "}&";

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("account/?" + q + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        res.status(500).send({"msg" : "Account not found"});
      }else{
        if (body.length > 0){

          q = "q={idAccount:" + req.params.idAccount + "}&s={date:-1,time:-1}&";

          httpClient.get("movement/?" + q + mLabAPIKey,
            function(err, resMLabM, bodyM){
              if(err){
                res.status(500).send({"msg" : "Error getting movements"});
              }else{
                if (bodyM.length > 0){
                  res.status(200).send(bodyM);
                }else{
                  res.status(404).send({"msg" : "No movements for this account"});
                }
              }

            });

        }else{
          res.status(404).send({"msg" : "Account not found"});
        }
      }

    });

}


function setMovementV1(req,res){
  console.log("POST /apiraqueljorge/v1/account/:idAccount/movement")

  var query = 'q={"idAccount" : "'+req.params.idAccount+'"}';
  var httpClient = requestJson.createClient(baseMLabURL);
  var queryMaxId = 'f={id:1}&s={id:-1}&l=1';
  var newId = 0;
  //[INICIO] Obtenemos el max id de movimientos
  httpClient.get("movement?" + queryMaxId + '&' + mLabAPIKey,
    function(err, resMaxId, body){
      newId = body[0].id + 1;

      var queryBalance = "q={id:" + req.params.idAccount + ",idUser:" + req.idUser + "}&";

      var accountBalance = 0;
      //[INICIO] Obtenemos el balance de la cuenta para actualizarlo
      httpClient.get('account?' + queryBalance + '&' + mLabAPIKey,
        function(err, resBalance, bodyBalance){
          console.log("Obtenemos balance");
          if (err){
            res.status(500).send({"msg" : "Error getting movements"});
          }else{
            if (bodyBalance.length > 0){
              accountBalance = parseInt(bodyBalance[0].balance);
              console.log("balance="+accountBalance);
              //[INICIO] Actualizamos el balance de la cuenta
              var newBalance = parseInt(accountBalance) + parseInt(req.body.amount);
              var putBody = '{"$set":{"balance":' + newBalance + '}}';

              httpClient.put('account?' + queryBalance + '&' + mLabAPIKey, JSON.parse(putBody),
              function(err, resPutBalance, bodyPutBalance){
                if (err){
                  res.status(500).send({"msg" : "Error updating balance"});
                }else{
                  if (bodyPutBalance.n > 0){
                    //[INICIO] Insertamos el movimiento
                    var newMovement = {
                      "id" : newId,
                      "idAccount": parseInt(req.params.idAccount),
                    	"date":req.body.date,
                    	"time": req.body.time,
                    	"concept": req.body.concept,
                    	"amount": req.body.amount,
                    	"balance": newBalance,
                    	"tag": req.body.tag
                    }
                    httpClient.post('movement?' + mLabAPIKey, newMovement,
                      function(err, resPostMov, bodyPostMov){
                        if (err){
                          res.status(500).send({"msg" : "Error creating movement"})
                        }else{
                          console.log("Movement created");
                          res.status(200).send({"msg" : "Movement created"});
                        }
                      });
                    //[FIN] Insertamos el movimiento
                  }else{
                    res.status(404).send({"msg" : "Account not found"});
                  }
                }
              });
              //[FIN] Actualizamos el balance de la cuenta
            }
            else
              res.status(400).send({"msg" : "Account not found"})
          }//else err get account balance
        }
      );
      //[FIN] Obtenemos el balance de la cuenta para actualizarlo
  });
  //[FIN] Obtenemos el max id de movimientos
}


module.exports.getMovementsByIdAccountV1 = getMovementsByIdAccountV1;
module.exports.setMovementV1 = setMovementV1;
