require('dotenv').config();

const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type, tsec");
 next();
}

app.use(express.json()); 
app.use(enableCORS);

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const movementController = require('./controllers/MovementController');
const accountController = require('./controllers/AccountController');
const tknService = require('./services/TokenServices');

app.listen(port);

console.log("Escuchando en el puerto " + port);

/***************AUTH*****************************/
//Login de un usuario
app.post('/apiraqueljorge/v1/login', authController.loginV1);
app.post('/apiraqueljorge/v1/logout/', tknService.isAuthenticatedV1, authController.logoutV1);

/***************USER*************************/
//Alta de un usuario
app.post('/apiraqueljorge/v1/user', userController.setUserV1);
//Consulta datos Usuario
app.get('/apiraqueljorge/v1/user', tknService.isAuthenticatedV1, userController.getUserByIdV1);

/**************ACCOUNTS******************/
app.post('/apiraqueljorge/v1/account/', tknService.isAuthenticatedV1, accountController.setAccountV1);
app.get('/apiraqueljorge/v1/account/:idAccount', tknService.isAuthenticatedV1, accountController.getAccountByIdV1);
app.get('/apiraqueljorge/v1/account/', tknService.isAuthenticatedV1, accountController.getUserAccounts);
app.delete('/apiraqueljorge/v1/account/:idAccount', tknService.isAuthenticatedV1, accountController.deleteAccountV1);


/**************MOVEMENTS******************/
//app.post('/apiraqueljorge/v1/movement/:id', tknService.isAuthenticatedV1, movementController.getMovementsByIdAccountV1);
app.get('/apiraqueljorge/v1/account/:idAccount/movement/', tknService.isAuthenticatedV1, movementController.getMovementsByIdAccountV1);
app.post('/apiraqueljorge/v1/account/:idAccount/movement/', tknService.isAuthenticatedV1, movementController.setMovementV1);
